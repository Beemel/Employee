EMPLOYEE ASSIGNMENT FOR SCANIA RESKILLING 2022

This app is built with Java and the Spring API as an assignment for Scania Reskilling - Back end developer course.
It is more or less fully built with IntelliJ IDEA 2021.3.2 (Ultimate Edition)


HOW TO GET GOING
Download code from GitLab, 
Unpack if applicable.
Open folder in intelliJ.
Run program.

- For non intelliJ user please refer to corresponding documentation in regards how to run program from your environment.

Functions are all meant to be testable by using Postman.
- available here https://www.postman.com/downloads/

If Postman is unknown to you please refer to
https://buildmedia.readthedocs.org/media/pdf/postman-quick-reference-guide/latest/postman-quick-reference-guide.pdf.
or
https://www.srijan.net/resources/blog/manual-api-testing-using-postman



BASIC FUNCTION AND EXPECTED RESULT
Requests of different types (more details under AVAILABLE FEATURES) are to be sent to the program which will deal
with these requests, and react and respond accordingly.
Most often the application will respond with a JSON body or a string
Some Requests from user/client side require JSON body as input.
Some functions will return null ("1" in Postman).
HTTP statuses should also be in accordance.



AVAILABLE FEATURES

Display all Employees:
Method GET:
        http://localhost:8080/employees
        - Returns all employees currently in memory (program hands three default Employees)

Display specific Employee
Method GET:
        http://localhost:8080/employees/x  (Where x is a number (example http://localhost:8080/employees/2))
        - Returns the Employee with coresponding Key/Id

Display device summary per Employee
Method GET:
        at http://localhost:8080/employees/devices/summary
        - Returns all Employees with full name and the number of devices that is assigned to them.

Display Employees hired within the last two years:
Method GET:
        http://localhost:8080/employees/new
        - Returns employees that has been hired within the last two years. With Employee Code and name

Display Full time Employees birthday and days left to next birthday
Method GET:
        http://localhost:8080/employees/birthdays
        - Returns Only employees that are full time. show full name, date of birth and amount of days until their birthday.

Add new Employee
Method POST:
        http://localhost:8080/employees/
        - Adds the users (JSON) input to the employee rooster.
          Exact JSON format to use for input could be copy pasted from as shown in "get employee" from above.

Replace Employee
Method PUT:
        http://localhost:8080/employees/x  (Where x is a number (example http://localhost:8080/employees/2))
        - Replace the user with the (JSON) input from Postman.
          Exact JSON format to use for input could be copy pasted from as shown in "get employee" from above.
          Requires x to be an existing Key/ID in this program Put is not able to create new Employees. for that please refer to Post


Edit/update/change data for specific Employee
Method PATCH
        http://localhost:8080/employees/ x  (Where x is a number (example http://localhost:8080/employees/2))
        Works in the same manner as Put but can be used to change only specific data fields. For PUT all data fields is required. In Patch only the once you choose.
        Field Employee Code is required for all patches.



If you wish to add, remove and/or edit devices for a certain employee use Patch as described above.

If changing or adding Employees the Employee Code must be a valid Luhn number
refer to https://en-academic.com/dic.nsf/enwiki/315051 for further info regarding Luhn.

Enjoy!

Best Regards,
Morgan Engström