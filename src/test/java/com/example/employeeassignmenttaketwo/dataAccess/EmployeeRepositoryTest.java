package com.example.employeeassignmenttaketwo.dataAccess;
import com.example.employeeassignmenttaketwo.models.domain.Employee;
import com.example.employeeassignmenttaketwo.models.dto.DaysToBirthdayDto;
import com.example.employeeassignmenttaketwo.models.dto.RecentlyHiredDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeRepositoryTest {
    EmployeeRepository employeeRepository;

    @BeforeEach
    void init() {
        employeeRepository = new EmployeeRepository();

        Employee employeeForTest = (new Employee(
                4,
                "firstName",
                "lastName",
                LocalDate.of( 1900 , 1 , 1 ),
                86L,
                LocalDate.of( 2021 , 1 , 1 ),
                "position",
                "formOfEmployment",
                List.of("device1", "device2")));
   }

    @Test
    void getAllEmployees() {

        //Arrange
        int sizeFromSeed = 3;

        //Act
        int realSize = employeeRepository.getAllEmployees().size();

        //Assert
        assertEquals(sizeFromSeed, realSize);
    }

    @Test
    void getEmployee() {
        //Arrange
        int expectedId = 1;
        String expectedFirstName = "John";
        //Act
        Employee employeeOne = employeeRepository.getEmployee(expectedId);
        //Assert
        assertEquals(expectedId, employeeOne.getId());
        assertEquals(expectedFirstName, employeeOne.getFirstName());
    }

    @Test
    void addEmployee() {
       //Arrange

        String newEmployeeFirstName= "firstName";

        Employee employeeForTest = (new Employee(
                4,
                "firstName",
                "lastName",
                LocalDate.of( 1900 , 1 , 1 ),
                86L,
                LocalDate.of( 2021 , 1 , 1 ),
                "position",
                "formOfEmployment",
                List.of("device1", "device2")));

        //Act
        Employee employeeInTest = employeeRepository.addEmployee(employeeForTest);

        //Assert
        assertEquals(newEmployeeFirstName, employeeInTest.getFirstName());
    }

    @Test
    void replaceEmployee() {
        //Arrange
        int replacedEmployeeID = 1;
        String replacedEmployeeFirstName = "firstName";

        Employee employeeForTest = (new Employee(
                1,
                "firstName",
                "lastName",
                LocalDate.of( 1900 , 1 , 1 ),
                86L,
                LocalDate.of( 2021 , 1 , 1 ),
                "position",
                "formOfEmployment",
                List.of("device1", "device2")));

        //Act

        Employee employeeInTest = employeeRepository.replaceEmployee(employeeForTest);

        //Assert
        assertEquals(replacedEmployeeID, employeeInTest.getId());
        assertEquals(replacedEmployeeFirstName, employeeInTest.getFirstName());
    }

    @Test
    void modifyEmployee() {
        //Arrange
        int replacedEmployeeID = 1;
        String replacedFirstName= null;
        String replacedLastName= "test";

        Employee employeeForTest = (new Employee(
                1,
                null,
                "test",
                LocalDate.of( 1900 , 1 , 1 ),
                86L,
                LocalDate.of( 2021 , 1 , 1 ),
                null,
                "",
                List.of("device1", "device2")));

        //Act

        Employee employeeInTest = employeeRepository.modifyEmployee(employeeForTest);

        //Assert
        assertEquals(replacedEmployeeID, employeeInTest.getId());
        assertEquals(replacedFirstName, employeeInTest.getFirstName());
        assertEquals(replacedLastName, employeeInTest.getLastName());
    }



    @Test
    void deleteEmployee() {
        //Arrange
        int expectedSize = 2;
        //Act
        employeeRepository.deleteEmployee(1);
        int newSize = employeeRepository.getAllEmployees().size();

        //Assert
        assertEquals(expectedSize, newSize);
    }

    @Test
    void employeeExists() {
        //Arrange
        Boolean expectedOutcomeTrue = true;
        Boolean expectedOutcomeFalse = false;

        //Act
        boolean resultTrue = employeeRepository.employeeExists(1);
        boolean resultFalse = employeeRepository.employeeExists(4);

        //Assert
        assertEquals(expectedOutcomeTrue, resultTrue);
        assertEquals(expectedOutcomeFalse, resultFalse);

    }

    @Test
    void getRecentlyHired() {

        //Arrange
        int expectedSize = 1;

        //Act
        RecentlyHiredDto recentlyHired = employeeRepository.getRecentlyHired();
        List test = List.of(recentlyHired);
        int actualSize = test.size();


        //Assert
        assertEquals(expectedSize, actualSize);
    }

    @Test
    void getDaysToBirthday() {

        //Arrange
        String expectedName = "Jane";
        String unExpectedName = "John";
        int expectedNumberOfDays = 36;
        boolean a=true;
        boolean b=false;
        boolean c=false;
        boolean d=true;
        boolean e=false;
        boolean f=false;


        //Act
        DaysToBirthdayDto daysToBirthday = employeeRepository.getDaysToBirthday();
        List birthdayList = daysToBirthday.getDaysToBirthday();
        String testString1 = birthdayList.get(0).toString();
        String testString2 = birthdayList.get(1).toString();


        boolean testA = testString1.contains(expectedName);
        boolean testB = testString2.contains(Integer.toString(expectedNumberOfDays));
        boolean testC = testString2.contains(expectedName);
        boolean testD = testString1.contains(Integer.toString(expectedNumberOfDays));
        boolean testE = testString2.contains(unExpectedName);
        boolean fTest = testString1.contains(unExpectedName);

        //Assert
        assertEquals(a, testA);
        assertEquals(b, testB);
        assertEquals(c, testC);
        assertEquals(d, testD);
        assertEquals(e, testE);
        assertEquals(f, fTest);
    }

    @Test
    void luhnEvaluator() {
        //Arrange
        Long validCode=84731L;
        Long invalidCode=12345L;

        //Act
        boolean valid = employeeRepository.luhnEvaluator(validCode);
        boolean invalid = employeeRepository.luhnEvaluator(invalidCode);

        //Assert
        assertEquals(true, valid);
        assertEquals(false, invalid);
    }
}