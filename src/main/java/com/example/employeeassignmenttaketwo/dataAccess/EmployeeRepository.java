package com.example.employeeassignmenttaketwo.dataAccess;

import com.example.employeeassignmenttaketwo.models.domain.Employee;
import com.example.employeeassignmenttaketwo.models.dto.DaysToBirthdayDto;
import com.example.employeeassignmenttaketwo.models.dto.RecentlyHiredDto;
import com.example.employeeassignmenttaketwo.models.maps.CountDevicesDtoMapper;
import com.example.employeeassignmenttaketwo.models.maps.DaysToBirthdayDtoMapper;
import com.example.employeeassignmenttaketwo.models.maps.RecentlyHiredDtoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

import java.util.*;

@Component
public class EmployeeRepository implements IEmployeeRepository {
    private Map<Integer, Employee> employees = seedEmployees();
    private CountDevicesDtoMapper countDevicesDtoMapper;

    @Autowired
    public EmployeeRepository(CountDevicesDtoMapper countDevicesDtoMapper) {
        this.countDevicesDtoMapper = countDevicesDtoMapper;
    }
    //No idea what this actually do but couldn't get my Unit test to work if I did not have it....
    public EmployeeRepository() {
        this.employees=seedEmployees();
    }

    //Setting initial values from given template, Unsure if it is ok to break the lines for the input here.
    //i think it gets better readability. I dont know if the missing field names for dates and list do matter.
    //did not get it to show up for LocalDate and List.of. Works fine though.
    private Map<Integer, Employee> seedEmployees() {
        var employees = new HashMap<Integer, Employee>();

        employees.put(1, new Employee(
                1,
                "John",
                "Doe",
                LocalDate.of( 1980 , 2 , 14 ),
                2018269734L,
                LocalDate.of( 2021 , 3 , 31 ),
                "Tester",
                "Part-time",
                List.of("Dell XPS 13")));

        employees.put(2, new Employee(
                2,
                "Jane",
                "Doe",
                LocalDate.of( 1984 , 5 , 17 ),
                1634751505L,
                LocalDate.of(2018 , 12 , 1 ),
                "Manager",
                "Full-time",
                List.of("Alienware X17","Samsung 30-inch LED Moitor")));

        employees.put(3, new Employee(
                3,
                "Sven",
                "Svensson",
                LocalDate.of( 1987 , 3,  22),
                1507119608L,
                LocalDate.of( 2019 , 2 , 15 ),
                "Developer",
                "Full-time",
                List.of("Dell XPS 15", "Samsung 27-inch LED Monitor")));

        return employees;
    }

    @Override
    public List<Employee> getAllEmployees() {
        return employees.values().stream().toList();
    }

    @Override
    public Employee getEmployee(int id) {
        return employees.get(id);
    }

    @Override
    public Employee addEmployee(Employee employee) {
        employees.put(employees.size()+1, employee);

        return getEmployee(employee.getId());
    }

    @Override
    public Employee replaceEmployee(Employee employee) {
        var employeeToDelete = getEmployee(employee.getId());
        employees.remove(employeeToDelete);
        employees.put(employee.getId(), employee);
        return getEmployee(employee.getId());
    }

    @Override
    public Employee modifyEmployee(Employee employee) {
        var employeeToModify = getEmployee(employee.getId());

        employeeToModify.setFirstName(employee.getFirstName());
        employeeToModify.setLastName(employee.getLastName());
        employeeToModify.setDateOfBirth(employee.getDateOfBirth());
        employeeToModify.setEmployeeCode(employee.getEmployeeCode());
        employeeToModify.setDateHired(employee.getDateHired());
        employeeToModify.setPosition(employee.getPosition());
        employeeToModify.setFormOfEmployment(employee.getFormOfEmployment());
        employeeToModify.setDevices(employee.getDevices());

        return employeeToModify;
    }

    @Override
    public void deleteEmployee(int id) {
        employees.remove(id);
    }

    @Override
    public boolean employeeExists(int id) {
        return employees.containsKey(id);
    }

    @Override
    public RecentlyHiredDto getRecentlyHired() {
        return RecentlyHiredDtoMapper.newlyHired(employees);
    }

    @Override
    public DaysToBirthdayDto getDaysToBirthday() {
        return DaysToBirthdayDtoMapper.daysToBirthdayDtoMapper(employees);
    }

    //This is a clusterfuck I am well aware of that. But it just refused to work out. So I accepted it for what it became.
    //Don't judge to harshly. The loop should probably be inside the Dto class. but I just didn't get it to work...
    @Override
    public List getCountDevices() {
            List outputList = new ArrayList<>();
            for (int i = 1; i < employees.size()+1; i++) {
            var deviceEmployee = employees.get(i);
            outputList.add (countDevicesDtoMapper.countDevicesDtoMapper(deviceEmployee));
            }
        return outputList;
    }

    public boolean isValidEmployee(Employee employee) {
        return employee.getId() > 0
                && employee.getFirstName()!= null
                && employee.getLastName()!= null
                && employee.getDateOfBirth()!= null
                && employee.getEmployeeCode()!= 0
                && employee.getDateHired()!= null
                && employee.getPosition()!= null
                && employee.getFormOfEmployment()!= null
                && employee.getDevices() != List.of(" ");
    }

    public boolean isValidEmployee (Employee employee, int id) {
        return isValidEmployee(employee) && employee.getId()==id;
    }

    private LocalDate dateRightNow(){
            LocalDate dateRightNow = LocalDate.now();
            return dateRightNow;
    }

    //Probably should have done a separate Class for this...
    //this is also very much "me". I know that there is probably a lot smarter, shorter and more aesthetically pleasing ways to write this.
    //But I went with what I know, and refused to google this (since this was the part I enjoyed the most playing around with.
    @Override
    public boolean luhnEvaluator(Long employeeCode) {
        String stringStartValue = employeeCode.toString();
        StringBuilder reversed = new StringBuilder(stringStartValue).reverse(); // ease the loop logic by reversing number (i.e. so it can work from left to right and have positive increments in the loop.
        int controlNumber = Integer.parseInt(reversed.substring(0, 1)); //set the "first" number (i.e. the last from original String, as it is reversed) as controlNumber

        //initialize variables
        int temporaryCheckSum = 0;
        int CheckSum =0;

        for (int i = 1; i < reversed.length() ; i++) { //loop through the number in accordance with Luhn principals. https://en-academic.com/dic.nsf/enwiki/315051
            int correspondingLuhnNumber = 0; //initialize and reset the number that is generated for each iteration.


            if (i == 1 || i % 2 != 0) { //for doubling numbers according to Luhn principal.
                int currentNumber = Integer.parseInt(reversed.substring(i, i + 1));
                currentNumber = currentNumber*2;


                while (currentNumber > 0) {  // mod 10 the current doubled number to get a single digit.
                    correspondingLuhnNumber = correspondingLuhnNumber + currentNumber % 10;
                    currentNumber = currentNumber / 10;
                }

            } else { //for adding the value of the numbers that are not to be doubled.
                correspondingLuhnNumber = Integer.parseInt(reversed.substring(i, i + 1));
            }

            temporaryCheckSum = temporaryCheckSum+correspondingLuhnNumber; //adding up the numbers from the two cases above

        }
        while (temporaryCheckSum >9) { //mod the tempChecksum to single digit.
            CheckSum = CheckSum + temporaryCheckSum % 10;
            temporaryCheckSum = temporaryCheckSum / 10;

        }
        int finalCheckSum = (10-CheckSum) %10; // achieving the finalChecksum in accordance with Luhn algorithm

        if (finalCheckSum == controlNumber){ //test finalCheckSum against the control number.
            return true;

        }
        return false;
    }

}