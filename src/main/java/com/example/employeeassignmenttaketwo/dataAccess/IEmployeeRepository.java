package com.example.employeeassignmenttaketwo.dataAccess;

import com.example.employeeassignmenttaketwo.models.domain.Employee;
import com.example.employeeassignmenttaketwo.models.dto.CountDevicesDto;
import com.example.employeeassignmenttaketwo.models.dto.DaysToBirthdayDto;
import com.example.employeeassignmenttaketwo.models.dto.RecentlyHiredDto;

import java.util.List;

//interfaces duh
public interface IEmployeeRepository {

    List<Employee> getAllEmployees();
    Employee getEmployee(int id);
    Employee addEmployee(Employee employee);
    Employee replaceEmployee (Employee employee);
    Employee modifyEmployee(Employee employee);
    void deleteEmployee(int id);
    boolean employeeExists(int id);
    RecentlyHiredDto getRecentlyHired();
    boolean luhnEvaluator(Long employeeCode);
    DaysToBirthdayDto getDaysToBirthday();
    List <CountDevicesDto> getCountDevices();
}
