package com.example.employeeassignmenttaketwo.models.maps;

import com.example.employeeassignmenttaketwo.models.domain.Employee;
import com.example.employeeassignmenttaketwo.models.dto.RecentlyHiredDto;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class RecentlyHiredDtoMapper {

    private static Boolean withinTwoYears(LocalDate dateHired) {
        //setting variable for comparison
         LocalDate dateHiredTemp = dateHired;

         //did keep LocalDate.now().minusYears(2) out of variable since the name is easy to interpret
        return dateHiredTemp.isAfter(LocalDate.now().minusYears(2));
    }

    public static RecentlyHiredDto newlyHired (Map<Integer, Employee> employees) {

        List<String> recentlyHired = new ArrayList<>();
        for (Map.Entry<Integer, Employee> thisEmployee : employees.entrySet()) {

            LocalDate dateHired = thisEmployee.getValue().getDateHired();
            String firstName = thisEmployee.getValue().getFirstName();
            String lastName = thisEmployee.getValue().getLastName();
            Long employeeCode = thisEmployee.getValue().getEmployeeCode();

            if (withinTwoYears(dateHired)) {
                recentlyHired.add(employeeCode +": "+ firstName + " " + lastName);
            }
        }
        return new RecentlyHiredDto(recentlyHired);
    }
}
