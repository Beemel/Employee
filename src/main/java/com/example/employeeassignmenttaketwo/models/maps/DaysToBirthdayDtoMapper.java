package com.example.employeeassignmenttaketwo.models.maps;

import com.example.employeeassignmenttaketwo.models.domain.Employee;
import com.example.employeeassignmenttaketwo.models.dto.DaysToBirthdayDto;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;


@Component
public class DaysToBirthdayDtoMapper {

        private static String daysToBirthday(LocalDate birthDate) {
            //setting dates that are to be compared.
            LocalDate dateOfBirth = birthDate;
            LocalDate nextBirthday = dateOfBirth.withYear(LocalDate.now().getYear());

            //resetting date to next year when this year's birthday already has been passed
            if (nextBirthday.isBefore(LocalDate.now()) || nextBirthday.isEqual(LocalDate.now())) {
                nextBirthday = nextBirthday.plusYears(1);
            }

              return String.valueOf(ChronoUnit.DAYS.between(LocalDate.now(),nextBirthday));
        }

        public static DaysToBirthdayDto daysToBirthdayDtoMapper(Map<Integer, Employee> employees) {

            List daysToBirthday = new ArrayList();
            for (Map.Entry<Integer, Employee> entry : employees.entrySet()) {
                LocalDate birthday = entry.getValue().getDateOfBirth();
                String employmentForm = entry.getValue().getFormOfEmployment();

                // kept the entry.getValue.getXxxxxName() here since the "name part was pretty selfexplanatory imo.
                if (employmentForm.equals("Full-time")) {
                    daysToBirthday.add(entry.getValue().getFirstName() + " " + entry.getValue().getLastName() +" was born " +birthday+ ". Which means that we'll celebrate the birthday " + daysToBirthday(birthday) +" days");
                  }
            }
            return new DaysToBirthdayDto(daysToBirthday);
        }
    }





