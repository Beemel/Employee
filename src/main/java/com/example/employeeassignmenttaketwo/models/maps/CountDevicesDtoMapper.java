package com.example.employeeassignmenttaketwo.models.maps;
import com.example.employeeassignmenttaketwo.models.domain.Employee;
import com.example.employeeassignmenttaketwo.models.dto.CountDevicesDto;
import org.springframework.stereotype.Component;

//See comment in Repo =(
@Component
public class CountDevicesDtoMapper {

    public CountDevicesDto countDevicesDtoMapper (Employee employee){

        //Set variables
        var firstName = new CountDevicesDto(employee.getFirstName(), employee.getId());
        var lastName = new CountDevicesDto(employee.getLastName(), employee.getId());
        String firstNameString = firstName.getFullName();
        String lastNameString = lastName.getFullName();
        String fullName = firstNameString + " "+ lastNameString;
        var deviceCount = new CountDevicesDto(fullName, employee.getDevices().size());


        return deviceCount;
        }
    }


