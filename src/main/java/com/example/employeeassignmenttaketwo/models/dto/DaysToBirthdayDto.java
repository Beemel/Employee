package com.example.employeeassignmenttaketwo.models.dto;

import java.util.List;

//This is a POJO, not much to say about it Fields with coresponding getters and setters
public class DaysToBirthdayDto {
    private List daysToBirthday;


    public DaysToBirthdayDto(List daysToBirthday) {
        this.daysToBirthday = daysToBirthday;
    }

    public List getDaysToBirthday() {
        return daysToBirthday;
    }

    public void setDaysToBirthday(List daysToBirthday) {
        this.daysToBirthday = daysToBirthday;
    }
}