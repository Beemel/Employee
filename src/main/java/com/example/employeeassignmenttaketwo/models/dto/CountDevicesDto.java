package com.example.employeeassignmenttaketwo.models.dto;

//This is a POJO, not much to say about it Fields with coresponding getters and setters
public class CountDevicesDto  {
    private String fullName;
    private int numberOfDevices;

    public CountDevicesDto(String fullName, int numberOfDevices) {
        this.fullName = fullName;
        this.numberOfDevices = numberOfDevices;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public int getNumberOfDevices() {
        return numberOfDevices;
    }

    public void setNumberOfDevices(int numberOfDevices) {
        this.numberOfDevices = numberOfDevices;
    }
}