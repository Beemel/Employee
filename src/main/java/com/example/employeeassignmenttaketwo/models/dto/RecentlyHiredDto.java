package com.example.employeeassignmenttaketwo.models.dto;

import java.util.List;

//This is a POJO, not much to say about it Fields with coresponding getters and setters
public class RecentlyHiredDto {
    private List<String> recentlyHired;

    public RecentlyHiredDto(List<String> recentlyHired) {
        this.recentlyHired = recentlyHired;
    }

    public List<String> getRecentlyHired() {
        return recentlyHired;
    }

    public void setRecentlyHired(List<String> recentlyHired) {
        this.recentlyHired = recentlyHired;
    }
}