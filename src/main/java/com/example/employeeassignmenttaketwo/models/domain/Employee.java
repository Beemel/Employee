package com.example.employeeassignmenttaketwo.models.domain;

import java.time.LocalDate;
import java.util.List;

//This is a POJO, not much to say about it Fields with coresponding getters and setters
public class Employee {

    private int id;
    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;
    private Long employeeCode;
    private LocalDate dateHired;
    private String position;
    private String formOfEmployment;
    private List devices;


    public Employee(int id, String firstName, String lastName, LocalDate dateOfBirth, Long employeeCode, LocalDate dateHired, String position, String formOfEmployment, List devices) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.employeeCode = employeeCode;
        this.dateHired = dateHired;
        this.position = position;
        this.formOfEmployment = formOfEmployment;
        this.devices = devices;
    }

    public int getId() {
        return id;
    }

    //SetId never used, keep it for possible future need
    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Long getEmployeeCode() {
        return employeeCode;
    }

    public void setEmployeeCode(Long employeeCode) {
        this.employeeCode = employeeCode;
    }

    public LocalDate getDateHired() {
        return dateHired;
    }

    public void setDateHired(LocalDate dateHired) {
        this.dateHired = dateHired;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getFormOfEmployment() {
        return formOfEmployment;
    }

    public void setFormOfEmployment(String formOfEmployment) {
        this.formOfEmployment = formOfEmployment;
    }

    public List getDevices() {
        return devices;
    }

    public void setDevices(List devices) {
        this.devices = devices;
    }
}





