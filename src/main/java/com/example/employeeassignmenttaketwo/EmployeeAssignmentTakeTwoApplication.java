package com.example.employeeassignmenttaketwo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmployeeAssignmentTakeTwoApplication {

    public static void main(String[] args) {
        SpringApplication.run(EmployeeAssignmentTakeTwoApplication.class, args);
    }

}
