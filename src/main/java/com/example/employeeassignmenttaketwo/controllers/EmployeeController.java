package com.example.employeeassignmenttaketwo.controllers;

import com.example.employeeassignmenttaketwo.dataAccess.EmployeeRepository;
import com.example.employeeassignmenttaketwo.models.domain.Employee;
import com.example.employeeassignmenttaketwo.models.dto.CountDevicesDto;
import com.example.employeeassignmenttaketwo.models.dto.DaysToBirthdayDto;
import com.example.employeeassignmenttaketwo.models.maps.RecentlyHiredDtoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//Setting up controllers to cater for different available inputs from Client
//Corresponding HTTP Statues added where applicable, not 100% sure I used them correctly, I used the one I thought was best suited.

@RestController
@RequestMapping("/employees")
public class EmployeeController {
    private EmployeeRepository employeeRepository;

    @Autowired
    public EmployeeController(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @GetMapping
    public ResponseEntity<List<Employee>> getAllEmployees() {
        return new ResponseEntity<>(employeeRepository.getAllEmployees(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Employee> getEmployee(@PathVariable int id) {
        if (!employeeRepository.employeeExists(id)) {
            return new ResponseEntity("Employee with id " + id + " does not exist", HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(employeeRepository.getEmployee(id), HttpStatus.OK);
    }

    @PostMapping
    //Lots of possible errors here. I think i catched most of them.
    //However, Testing the "isValidEmployee" first does not seem to eliminate problem that occurs when the Luhn field name is incorrect.

    public ResponseEntity<String> addEmployee(@RequestBody Employee employee) {

        if (!employeeRepository.isValidEmployee(employee)) {
            return new ResponseEntity<>("That input is not valid. one ore more parameters have inconsistencies from what is expected", HttpStatus.BAD_REQUEST);
        }
        if (!employeeRepository.luhnEvaluator(employee.getEmployeeCode())) {
            return new ResponseEntity<>("That Employee Code does not pass Luhn Validation", HttpStatus.NOT_ACCEPTABLE);
        }

        if (employeeRepository.employeeExists(employee.getId())) {
            return new ResponseEntity<>("That ID already exist, Please use next valid id which is "+(employeeRepository.getAllEmployees().size()+1), HttpStatus.ALREADY_REPORTED);
        }
        if(employeeRepository.getAllEmployees().size()+1 != employee.getId()){
            return new ResponseEntity<>("Please add Employees in order, Next valid id to enter is " +(employeeRepository.getAllEmployees().size()+1) , HttpStatus.EXPECTATION_FAILED);
        }

        employeeRepository.addEmployee(employee);
        return new ResponseEntity<>("New employee has been created", HttpStatus.CREATED);
    }


    @PutMapping("/{id}")

    public ResponseEntity<String> replaceEmployee(@PathVariable int id, @RequestBody Employee employee) {
        if (!employeeRepository.isValidEmployee(employee, id)) {
            return new ResponseEntity<>("The format of the request is not valid", HttpStatus.BAD_REQUEST);
        }

        if (!employeeRepository.luhnEvaluator(employee.getEmployeeCode())) {
            return new ResponseEntity<>("That Employee Code does not pass Luhn Validation", HttpStatus.NOT_ACCEPTABLE);
        }

        if (!employeeRepository.employeeExists(id)) {
            return new ResponseEntity<>("This Employee ID does not exist", HttpStatus.NOT_FOUND);
        }

        employeeRepository.replaceEmployee(employee);
        return new ResponseEntity<>("Employee has been replaced", HttpStatus.ACCEPTED);
    }

    @PatchMapping("/{id}")

    public ResponseEntity<Employee> modifyEmployee(@PathVariable int id, @RequestBody Employee employee) {

        //Don't know how and if it is possible. But since I wanted to return the Employee here I didn't manage to get a String return to the Luhn Validation.
        if (!employeeRepository.luhnEvaluator(employee.getEmployeeCode())) {
            return new ResponseEntity<>(null, HttpStatus.NOT_ACCEPTABLE);
        }

        return new ResponseEntity<>(employeeRepository.modifyEmployee(employee), HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteEmployee(@PathVariable int id) {
        if (!employeeRepository.employeeExists(id)) {
            return new ResponseEntity<>("Employee with id " + id + " does not exist", HttpStatus.NOT_FOUND);
        }
        employeeRepository.deleteEmployee(id);
        return new ResponseEntity<>("Employee with id " + id + " has been deleted", HttpStatus.ACCEPTED);
    }

    //A bit unsure on when to use OK and when to use ACCEPTED. but I went with OK here.
    //tried to be consistent and using OK for the GET's and ACCEPTED for the other mappings.
    @GetMapping("/new")
    public ResponseEntity <List<RecentlyHiredDtoMapper>> getRecentlyHired() {
        return new ResponseEntity(employeeRepository.getRecentlyHired(), HttpStatus.OK);
    }

    @GetMapping("/birthdays")
    public ResponseEntity<List<DaysToBirthdayDto>> daysToBirthday() {
        return new ResponseEntity(employeeRepository.getDaysToBirthday(), HttpStatus.OK);
    }

    @GetMapping("/devices/summary")
    public ResponseEntity<List<CountDevicesDto>> getCountDevices() {
         return new ResponseEntity<>(employeeRepository.getCountDevices(), HttpStatus.OK);
    }
}

